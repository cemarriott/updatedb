#! /usr/bin/env python

''' 
UpdateDB.py
Copyright 2013 Clayton Marriott
Creates a database from lab usage data.
'''

from optparse import OptionParser
import httplib
import os
import re
import shutil
import sqlite3
import zipfile


usage = "usage: %prog [options]"
optParser = OptionParser(usage=usage)
optParser.add_option("-l", "--local", action="store_true", dest="local", default=False,
	help="Use local webstats directory. MUST be named 'webstats'.")
optParser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False,
	help="Output all ")
(options, args) = optParser.parse_args()


if options.verbose:
	print("-----------------------------------------")
	print("UsageDB - Copyright 2013 Clayton Marriott")
	print("-----------------------------------------")

### DOWNLOAD SECTION ###

if options.local == True:
	if options.verbose:
		print("Using local webstats directory.")
	if not os.path.lexists('webstats/'):
		print("webstats dir does not exist. Exiting.")
		exit(1)

else:
	if os.path.isfile('archive.zip'):
		if options.verbose:
			print("Found existing archive..")
		os.remove('archive.zip')
		if options.verbose:
			print("    Removed.")

	if os.path.lexists('webstats/'):
		if options.verbose:
			print("Webstats dir already exists..")
		shutil.rmtree('webstats/')
		if options.verbose:
			print("    Removed.")

	if options.verbose:
		print("Starting download..")

	# Connect to the server, get the directory list.
	# This is the root dir listing.
	conn = httplib.HTTPConnection("labstats.brandonjd.net")
	conn.request("GET", "/archive_full.zip")

	# Check response. Set file var.
	r1 = conn.getresponse()
	if r1.status != 200:
		print("Error: " + r1.status + " " + r1.reason)

	# Write the archive to outFile.
	outFile = open('archive.zip', 'wb')

	# Write all of file.
	while 1:
		readFile = r1.read()
		if not readFile:
			break
		outFile.write(readFile)

	outFile.close()
	if options.verbose:
		print("Archive download complete.")

	# Close connection when done.
	conn.close()

	if options.verbose:
		print("Starting extraction..")
	archiveFile = zipfile.ZipFile('archive.zip', 'r')
	archiveFile.extractall()
	archiveFile.close()

	print("Download/extraction complete.")





### DATABASE SECTION ###

# If database exists, remove it. Then create new.
if os.path.isfile('labstats.db'):
	if options.verbose:
		print("Found existing database, removing..")
	os.remove('labstats.db')

if options.verbose:
	print("Creating database.")
conn = sqlite3.connect('labstats.db')
c = conn.cursor()

if options.verbose:
	print("    Database created.")

if os.path.isdir('webstats/'):
	if options.verbose:
		print("webstats directory found.")
else:
	print("webstats directory not found. Exiting..")
	conn.close()
	exit(1)

labListCorrect = []

for directory, subdirectories, files in os.walk('webstats/'):
	for file in files:
		if file.endswith('.html'):
			if not "header" in file:
				currentPath = os.path.join(directory, file)

				parseFile = open(currentPath, 'r')

				# Get the date.
				entryDate = currentPath[9:19]

				# Get the time.
				entryTime = currentPath[31:39]

				date_time = entryDate + "_" + entryTime

				if options.verbose:
					print(date_time)

				labList = []
				numberList = []

				fileLines = parseFile.readlines()
				parseFile.close()

				for line in fileLines:
					
					# Don't grab the timestamp line.
					if "###" in line: break
					
					# Grab room name and number.
					parseCheck = re.findall('(?![">])[A-Za-z&;]+ [A-Za-z]{0,2}[0-9]+[-A-Za-z]{0,8}(?=</a>||</div)', line)
					if(parseCheck):
						labList.append(str(parseCheck))
						
					if not parseCheck:
						parseResult = re.findall('(?!"td">)[0-9]+(?=</div)', line)
						if (parseResult): 
							numberList.append(parseResult)

				if not numberList: break

				
				while (labList):
					lab = labList.pop(0)
					lab = lab.replace("&amp;","n")
					lab = lab.replace(" ","_")
					lab = lab.replace("'","")
					lab = lab.replace("-","_")

					if not lab in labListCorrect:
						if options.verbose:
							print("Creating " + lab)
						insertString = "CREATE TABLE " + lab + " (date_time text, inuse integer, avail integer, unavail integer, offline integer, total integer)"
						c.execute(insertString)
						conn.commit()
						labListCorrect.append(lab)

				currentList = labListCorrect[:]

				while (currentList):
					
					if len(numberList) == 0:
						break

					lab = currentList.pop(0)

					firstVal = numberList.pop(0)
					secondVal = numberList.pop(0)
					thirdVal = numberList.pop(0)
					fourthVal = numberList.pop(0)
					fifthVal = numberList.pop(0)

					infoString = "INSERT INTO " + lab + " VALUES ('" + date_time + "'," + firstVal[0] + "," + secondVal[0] + "," + thirdVal[0] + "," + fourthVal[0] + "," + fifthVal[0] + ")"
					c.execute(infoString)
				conn.commit()
conn.close()


### CLEANUP ###
if options.local == False:
	if os.path.isfile('archive.zip'):
		os.remove('archive.zip')

	if os.path.lexists('webstats/'):
		shutil.rmtree('webstats/')

print("Done.")
